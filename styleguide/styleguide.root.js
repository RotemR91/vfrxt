import Vue from "vue";
import Vuex from "vuex";
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'

import pkg from '../package.json'

// Vue.use(Vuetify)

// import { state, mutations, getters } from `../${pkg.nuxtDir}/store/mutations`;

Vue.use(Vuex);

// in order not to polute the database and making useless database calls
// we mock axios (we could have used mock-axios instead)
Vue.prototype.$axios = {
  $get:() => Promise.resolve([
    {employee_name:'success', employee_salary: "12"},
    {employee_name:'joe', employee_salary: "1789"},
    {employee_name:'bill', employee_salary: "13"},
    {employee_name:'martin', employee_salary: "1234"},
    {employee_name:'anette', employee_salary: "34"},
    {employee_name:'hilary', employee_salary: "567"},
  ])
};

// const store = new Vuex.Store({
//   state,
//   getters,
//   mutations
// });

export default previewComponent => {
  // https://vuejs.org/v2/guide/render-function.html
  return {
    // store,
    vuetify: new Vuetify({
      customVariables: [`./assets/variables.scss`],
      iconfont: 'mdi',
      icons: {
        iconfont: 'mdi',
      },
      theme: {
        dark: false,
        themes: {
          dark: {
            primary: colors.blue.darken2,
            accent: colors.grey.darken3,
            secondary: colors.amber.darken3,
            info: colors.teal.lighten1,
            warning: colors.amber.base,
            error: colors.deepOrange.accent4,
            success: colors.green.accent3
          },
        }
      }
    }),
    render(createElement) {
      return createElement(
        'v-app',
        {
          props: {
            id: 'v-app'
          }
        },
        [createElement(Object.assign(previewComponent))]
      )
    }
  };
};
