import axios from 'axios'

const actions = {
  updateTooltip({state, commit, getters}, {id, event, field}) {
    if (id === null) {
      commit('setTooltip', null)
      return
    }
    let data = {}
    data.card = getters.getCard(id)
    data.name = id
    data.x = event.clientX + 10
    data.y = event.clientY + 10
    commit('setTooltip', data)
  },

  async fetchKeywordCards({state, commit, getters}, {}) {
    try {
      let response = await axios.get('data/cards.min.json')
      if (response.data) {
        commit('addCards', response.data)
        commit('snackMessage', 'Card data retrieved')
      }
    } catch (e) {
      console.error(e)
      commit('snackMessage', 'Oh no, something went wrong! Please refresh the page')
    } finally {

    }
  }
}

export default actions
